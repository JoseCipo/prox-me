package com.programacaoweb.proxme.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.programacaoweb.proxme.service.TurmaService;

@RestController
@RequestMapping("/turma")
public class TurmaController {

	@Autowired
	private TurmaService turmaService;

	@GetMapping(path = "/{idCurso}")
	public ResponseEntity<?> buscarTurmasPeloIdCurso(@PathVariable Long idCurso) {
		return ResponseEntity.ok(turmaService.buscarTurmasPeloIdCurso(idCurso));
	}

}
