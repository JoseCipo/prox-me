package com.programacaoweb.proxme.adapter;

import org.springframework.stereotype.Component;

import com.programacaoweb.proxme.dto.TurmaDTO;
import com.programacaoweb.proxme.entity.Curso;
import com.programacaoweb.proxme.entity.Turma;

@Component
public class TurmaAdapter {

	public TurmaDTO toDto(Turma turma) {
		return TurmaDTO.builder().id(turma.getId()).nome(turma.getNome())
				.idCurso(turma.getCurso() != null ? turma.getCurso().getId() : null).build();
	}

	public Turma toEntity(TurmaDTO turmaDTO) {
		Curso curso = Curso.builder().id(turmaDTO.getIdCurso()).build();
		return Turma.builder().id(turmaDTO.getId()).nome(turmaDTO.getNome()).curso(curso).build();
	}

}
