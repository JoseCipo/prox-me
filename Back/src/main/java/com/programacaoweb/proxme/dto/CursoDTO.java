package com.programacaoweb.proxme.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursoDTO implements Serializable {

	private static final long serialVersionUID = 1134515286548613132L;

	private Long id;

	private String nome;

	private List<TurmaDTO> turmas;

}
